# Welcome to FxTsx!
## Demo
https://6480ca7164d3b257e22e7162-vbndxnxhnx.chromatic.com/?path=/docs/fxtsx-html-value-button--docs

## Development

From your terminal:

```sh
npm run dev
```

This starts your app in development mode, rebuilding assets on file changes.

## Deployment

First, build your app for production:

```sh
npm run build
```

Then run the app in production mode:

```sh
npm start
```

Now you'll need to pick a host to deploy it to.
